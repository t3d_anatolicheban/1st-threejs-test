import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'
import * as dat from 'lil-gui'
import gsap from 'gsap'
import { AxesHelper, Box3, BoxGeometry, ConeGeometry, CylinderGeometry, Group, Mesh, MeshBasicMaterial, MeshMatcapMaterial, SphereGeometry, TextureLoader, TorusGeometry, Vector3 } from 'three'

/**
 * Base
 */
// Debug
const gui = new dat.GUI()

//Params and Tweaks
const params = {
  inputX: 1,
  inputY: 2,
  inputZ: 1,
  generate() {
    generateObjects()
  },
  explode() {
    explodeObjects()
  },
  reset() {
    resetObjects()
  }
}

gui.add(params, 'inputX').min(1).step(1).max(50)
gui.add(params, 'inputY').min(1).step(1).max(50)
gui.add(params, 'inputZ').min(1).step(1).max(50)
gui.add(params, 'generate').name('Generate')
gui.add(params, 'explode').name('Explode')
gui.add(params, 'reset').name('Reset')

// Canvas
const canvas = document.querySelector('canvas.webgl')

// Scene
const scene = new THREE.Scene()

//Axes Helper
const axesHelper = new AxesHelper(60)
scene.add(axesHelper)

const size = 1;

const textureLoader = new TextureLoader()
const textures = [textureLoader.load('/textures/water.jpg'), textureLoader.load('/textures/fire.jpg'), textureLoader.load('/textures/ground.jpg')]

let objects;
const geometries = [new ConeGeometry(size / 2, size), new BoxGeometry(size, size, size), new SphereGeometry(size / 2, 20, 20), new CylinderGeometry(size / 2, size / 2, size)]
const material = new MeshMatcapMaterial()

function generateObjects() {

  if (objects !== null) {
    scene.remove(objects)
    objects = null
  }

  objects = new Group()

  const { inputX, inputY, inputZ } = params

  for (let x = 0; x < inputX; x++) {
    for (let y = 0; y < inputY; y++) {
      for (let z = 0; z < inputZ; z++) {
        const randomGeometryIndex = Math.floor(Math.random() * geometries.length)
        const randomTextureIndex = Math.floor(Math.random() * textures.length)

        const newMaterial = material.clone()
        newMaterial.map = textures[randomTextureIndex]

        const obj = new Mesh(geometries[randomGeometryIndex], newMaterial)


        obj.position.set(1 / 2 - inputX / 2 + x, 1 / 2 - inputY / 2 + y, 1 / 2 - inputZ / 2 + z)
        //Сохр в юзердату
        obj.userData.initialCoords = {}
        obj.userData.initialCoords.x = obj.position.x
        obj.userData.initialCoords.y = obj.position.y
        obj.userData.initialCoords.z = obj.position.z

        objects.add(obj)
      }
    }
  }


  scene.add(objects)
}

generateObjects()

function explodeObjects() {
  if (objects !== null) {
    for (let child of objects.children) {
      const randomCoordinates = {
        x: Math.cos(Math.random()) * randomIntFromInterval(-20, 20),
        y: Math.sin(Math.random()) * randomIntFromInterval(-20, 20),
        z: Math.cos(Math.random()) * randomIntFromInterval(-20, 20)
      }
      gsap.to(child.position, { ...randomCoordinates, duration: 1 })
    }
  }
}

function resetObjects() {
  if (objects !== null) {
    for (let child of objects.children) {
      gsap.to(child.position, { ...child.userData.initialCoords, duration: 1 })
    }
  }
}

function randomIntFromInterval(min, max) { // min and max included
  return Math.floor(Math.random() * (max - min + 1) + min)
}

/**
 * Sizes
 */
const sizes = {
  width: window.innerWidth,
  height: window.innerHeight
}

window.addEventListener('resize', () => {
  // Update sizes
  sizes.width = window.innerWidth
  sizes.height = window.innerHeight

  // Update camera
  camera.aspect = sizes.width / sizes.height
  camera.updateProjectionMatrix()

  // Update renderer
  renderer.setSize(sizes.width, sizes.height)
  renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
})

/**
 * Camera
 */
// Base camera
const camera = new THREE.PerspectiveCamera(75, sizes.width / sizes.height, 0.1, 100)
camera.position.x = 3
camera.position.y = 3
camera.position.z = 3
scene.add(camera)

// Controls
const controls = new OrbitControls(camera, canvas)
controls.enableDamping = true

/**
 * Renderer
 */
const renderer = new THREE.WebGLRenderer({
  canvas: canvas
})
renderer.setSize(sizes.width, sizes.height)
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))

/**
 * Animate
 */
const clock = new THREE.Clock()

const tick = () => {
  const elapsedTime = clock.getElapsedTime()

  // Update controls
  controls.update()

  // Render
  renderer.render(scene, camera)

  // Call tick again on the next frame
  window.requestAnimationFrame(tick)
}

tick()
